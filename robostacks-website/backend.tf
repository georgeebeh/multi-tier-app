# store the terraform state file in s3
terraform {
    backend "s3" {
        bucket = "robostacks-tfstate"
        key    = "robostacks-website.tfstate"
        region = "eu-west-2"
        profile = "default"
    }
}