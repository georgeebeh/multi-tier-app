# Create vpc
resource "aws_vpc" "vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support   = true
  tags = {
    Name = "${var.project-name}-vpc"
  }
}

# create internet gateway and attach to vpc
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "${var.project-name}-igw"
  }
}

# use data source to get all availability zones in the region
data "aws_availability_zones" "availability" {}

# create public subnet az1
resource "aws_subnet" "public_subnet_az1" {
  vpc_id = aws_vpc.vpc.id
  cidr_block = var.public_subnet_az1_cidr
  availability_zone = data.aws_availability_zones.availability.names[0]
  map_public_ip_on_launch = true

  tags = {
    Name = "${var.project-name}-public-subnet-az1"
  }
}

# create public subnet az2
resource "aws_subnet" "public_subnet_az2" {
  vpc_id = aws_vpc.vpc.id
  cidr_block = var.public_subnet_az2_cidr
  availability_zone = data.aws_availability_zones.availability.names[1]
  map_public_ip_on_launch = true

  tags = {
    Name = "${var.project-name}-public-subnet-az2"
  }
}


# create public subnet az3
resource "aws_subnet" "public_subnet_az3" {
  vpc_id = aws_vpc.vpc.id
  cidr_block = var.public_subnet_az3_cidr
  availability_zone = data.aws_availability_zones.availability.names[2]
  map_public_ip_on_launch = true

  tags = {
    Name = "${var.project-name}-public-subnet-az3"
  }
}



#create route table and add public route 
resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.vpc.id 

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
 }

 tags = {
      Name = "${var.project-name}-rtb"
    }
} 

# associate public subnet az1 with public route table
resource "aws_route_table_association" "public_subnet_az1_association" {
  subnet_id = aws_subnet.public_subnet_az1.id
  route_table_id = aws_route_table.public_route_table.id
}

# associate public subnet az2 with public route table
resource "aws_route_table_association" "public_subnet_az2_association" {
  subnet_id = aws_subnet.public_subnet_az2.id
  route_table_id = aws_route_table.public_route_table.id
}

# associate public subnet az3 with public route table
resource "aws_route_table_association" "public_subnet_az3_association" {
  subnet_id = aws_subnet.public_subnet_az3.id
  route_table_id = aws_route_table.public_route_table.id
}

#create private app subnet az1
resource "aws_subnet" "private_subnet_az1" {
  vpc_id = aws_vpc.vpc.id
  cidr_block = var.private_subnet_az1_cidr
  availability_zone = data.aws_availability_zones.availability.names[0]
  map_public_ip_on_launch = false

  tags = {
    Name = "${var.project-name}-private-subnet-az1"
  }
}

#create private app subnet az2
resource "aws_subnet" "private_subnet_az2" {
  vpc_id = aws_vpc.vpc.id
  cidr_block = var.private_subnet_az2_cidr
  availability_zone = data.aws_availability_zones.availability.names[1]
  map_public_ip_on_launch = false

  tags = {
    Name = "${var.project-name}-private-subnet-az2"
  }
}

#create private app subnet az3
resource "aws_subnet" "private_subnet_az3" {
  vpc_id = aws_vpc.vpc.id
  cidr_block = var.private_subnet_az3_cidr
  availability_zone = data.aws_availability_zones.availability.names[2]
  map_public_ip_on_launch = false

  tags = {
    Name = "${var.project-name}-private-subnet-az3"
  }
}

#create private data subnet az1
resource "aws_subnet" "private_data_subnet_az1" {
  vpc_id = aws_vpc.vpc.id
  cidr_block = var.private_data_subnet_az1_cidr
  availability_zone = data.aws_availability_zones.availability.names[0]
  map_public_ip_on_launch = false

  tags = {
    Name = "${var.project-name}-private-data_subnet-az1"
  }
}

#create private data subnet az2
resource "aws_subnet" "private_data_subnet_az2" {
  vpc_id = aws_vpc.vpc.id
  cidr_block = var.private_data_subnet_az2_cidr
  availability_zone = data.aws_availability_zones.availability.names[1]
  map_public_ip_on_launch = false

  tags = {
    Name = "${var.project-name}-private-data_subnet-az2"
  }
}


#create private data subnet az3
resource "aws_subnet" "private_data_subnet_az3" {
  vpc_id = aws_vpc.vpc.id
  cidr_block = var.private_data_subnet_az3_cidr
  availability_zone = data.aws_availability_zones.availability.names[2]
  map_public_ip_on_launch = false

  tags = {
    Name = "${var.project-name}-private-data_subnet-az3"
  }
}